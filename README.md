# README #

### Inspiration ###
Some areas in the world have not mobile data coverage, or the speed is too slow. For those cases, we though it could be useful for people to provide them a free service to search for any word they are looking for only by sending an SMS (you can see the whole process with the pictures slides).

### What it does ###
It basically let's the user scan a QR code from our website using their mobile and it automatically opens the SMS application with a phone number written on it (if the user already knows the phone number this step is not necessary). Then the user just needs to type the word/s he/she is looking for, and the application is going to make a search using Wikipedia API and retrieve the brief description of that word, and then send it back to the user by SMS.

### How we built it ###
We used Twilio API to register a phone number, and also to make the communication between any phone and the server, and from the server we call Wikipedia API. We used Node.JS to build it.

### Challenges we ran into ###
Using Twilio API. Trying to build an app anyone can use it with any mobile device. Making it easy to use for end-users.

### Accomplishments that we are proud of ###
Learn how to use Twilio API and Wikipedia API.

### What we learned ###
Twilio API, Wikipedia API, some useful Node.JS functions, working as a team.

### What's next for TextTome ###
We would like to give users more services, so they can freely subscribe to the service they want. For example, an automatic translation service using Google Translator API, or a public transportation API.

### Built With ###
node.js
html5
twilio

### Try it out ###
[treasure-sage.hyperdev.space](Link URL)